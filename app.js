var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');
var path = require('path');

var app = express();

const router = require('./routes/route');

const PORT = 9000;

// Connect Mongodb
mongoose.connect('mongodb://localhost:27017/locationList'); 

// on Connection
mongoose.connection.on('connected', ()=>{
    console.log('connected to database mongodb @ 27017');
});

// on Connection
mongoose.connection.on('error', (err)=>{
    if(err)
        console.log('Error in database connection:' + err);
});

app.use(cors());

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', router);

app.get('/', (req, res)=>{
    res.json({Name:'Hamza Jamil'});
})

app.listen(PORT, ()=>{
    console.log("Server is running at this PORT: " + PORT);
})