# GeoLocationApp

## Before You Begin
Before you begin we recommend you read about the basic building blocks that assemble a MEAN.JS application:
* MongoDB - Go through [MongoDB Official Website](http://mongodb.org/) and proceed to their [Official Manual](http://docs.mongodb.org/manual/), which should help you understand NoSQL and MongoDB better.

## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:
* Git - [Download & Install Git](https://git-scm.com/downloads). OSX and Linux machines typically have this already installed.
* Node.js - [Download & Install Node.js](https://nodejs.org/en/download/) and the npm package manager. If you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.
* MongoDB - [Download & Install MongoDB](http://www.mongodb.org/downloads), and make sure it's running on the default port (27017).

## Note
Verify that you are running at least Node.js version 8.x or greater and npm version 5.x or greater by running node -v and npm -v in a terminal/console window. Older versions produce errors, but newer versions are fine.

### Cloning The GitHub Repository

git clone https://Hamza945@bitbucket.org/Hamza945/geolocationapp.git

## Install APP

Open new terminal in you direction:

####For Server: e.g (Path: F:\Mean Project\geolocationapp)

To install the dependencies, run this in the application folder from the command-line:

$ npm install

After complete this run command: 

$ npm Start

####For Client: e.g (Path: F:\Mean Project\geolocationapp\client)

$ npm install

After complete this run command: 

$ npm Start

Now you can access your application at http://localhost:4200

