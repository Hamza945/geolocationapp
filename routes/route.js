var express = require('express');

const Location = require('../module/locations');

var router = express.Router();

// Get Location
router.get('/getLocations', (req, res, next)=>{
    Location.find((err, locations)=>{
        if(!err){
            res.json(locations);
        }
    })
})

// Add Location
router.post('/location', (req, res, next)=>{
    var newLocation = new Location({
        user_id: req.body.user_id,
        email: req.body.email,
        geoLocation: {
            city: req.body.geoLocation.city,
            country: req.body.geoLocation.country
        }
    })
    newLocation.save((err, location)=>{
        if(err)
            res.json({msg: "Failed to Add location"});
        else
            res.json({msg: "Succefully Added location"});
    })
})

// Delete Location
router.delete('/location/:id', (req, res, next)=>{
    Location.findByIdAndRemove(req.params.id, function (err, user) {
      console.log('deleting user', user);
      if (err)
        throw err;
      Location.remove({_id: req.param.id}, (err, result)=>{
        if(err)
            res.json({err: err, message: "Failed", success: false});
        else
          res.json({ success: true, message: "Deleted", res: result });
   });
  });
})

module.exports = router;