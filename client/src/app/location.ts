export class Location {
    user_id: String;
    email: String;
    geoLocation: {
        city: String;
        country: String;
    };
}
