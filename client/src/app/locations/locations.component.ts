import { Component, OnInit } from '@angular/core';
// import { Observable } from 'rxjs/Rx';

import { LocationsService } from '../locations.service';
import { Location } from '../location';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css'],
  providers: [LocationsService]
})

export class LocationsComponent implements OnInit {

  locations: Location[];
  location: Location;
  user_id: string;
  email: string;
  country: string;
  city: string;

  constructor(private locationsService: LocationsService) { }

  // Get Locations
  ngOnInit() {
    this.locationsService.getLocations().subscribe(
      (locations: Location[]) => {
        console.log(locations);
        this.locations = locations;
      }
    );
  }

  // Add New Location
  deleteLocation(_id: String) {
    this.locationsService.deleteLocations(_id).subscribe(res => {
      console.log('RES----- ' + res);
      this.ngOnInit();
    });
  }

  // Add New Location
  addLocation() {
    const newLocation = {
      user_id: this.user_id,
      email: this.email,
      geoLocation: {
        country: this.country,
        city: this.city
      }
    };
    this.locationsService.addLocations(newLocation).subscribe(result => {
      if (result) {
        console.log('RES----- ' + result);
        this.ngOnInit();
      }
    });
  }
}
