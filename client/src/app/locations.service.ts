import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from './location';

@Injectable({
  providedIn: 'root'
})

export class LocationsService {

  constructor(private http: HttpClient) { }

  // Get localtions
  getLocations() {
    return this.http.get('http://localhost:9000/api/getLocations');
    // .map(res => res.json());
  }
  // Add localtion
  addLocations(newlocation) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:9000/api/location', newlocation, { headers: headers });
    // .map(res => res.json());
  }
  // Delete localtion
  deleteLocations(id) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.delete('http://localhost:9000/api/location/' + id);
    //  .map(res => res.json());
  }
  // Update localtion
  // updateLocations(id){
  //   var headers = new headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.get('http://localhost:3000/api/location')
  //     .map(res => res.json());
  // }
}
