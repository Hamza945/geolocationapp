var mongoose = require('mongoose');

const locationSchema = mongoose.Schema({
    user_id:{
        type: String,
        require: true
    },
    email:{
        type: String,
        require: true
    },
    geoLocation:{
        city:{
            type: String,
            require: true
        },
        country:{
            type: String,
            require: true
        },
    }
})

const Location = module.exports = mongoose.model('Location', locationSchema);